/*jshint jquery: true*/

$().ready(function () {
    var receivedJSON;

    $("#playButton").click(function () {
        var gameData = {
            codeSize: $("#codeSize").val(),
            maxColors: $("#maxColors").val(),
            maxMoves: $("#maxMoves").val()
        };

        $.ajax({
            url: "/newgame",
            type: 'POST',
            data: JSON.stringify(gameData),
            contentType: 'application/json; charset=UTF-8',
            dataType: 'json',
            async: false,
            success: function () {}
        });
        $("#codeSize").prop('disabled', true);
        $("#maxColors").prop('disabled', true);
        $("#maxMoves").prop('disabled', true);
        $("#playButton").prop('disabled', true);

        for (var i = 0; i < gameData.codeSize; i++) {
            $("<input type='text' id='zgaduj' class='form-control'/>").insertAfter(".generateAnswerField");
        }

        $("#sendAnswerField").show();
    });

    $("#sendAnswerField").click(function () {
        var inserted = {
            move: []
        };

        $("#allField input[id='zgaduj']").each(function () {
            inserted.move.push($(this).val());
        });

        $.ajax({
            url: '/move',
            type: 'POST',
            data: JSON.stringify(inserted),
            contentType: 'application/json; charset=UTF-8',
            dataType: 'json',
            async: false,
            success: function (serverData) {
                receivedJSON = serverData;
            }
        });

        $("<p>Ruch gracza: " + inserted.move + "</p>").insertAfter(".generateAnswerField");
        $("<p>Czarne: " + receivedJSON.black + ", Biały: " + receivedJSON.whites + "                 </p>").insertAfter(".generateAnswerField");

        if (receivedJSON.black === receivedJSON.codeSize) {
            $("<p>Udało ci się wygrać :)</p>").insertAfter(".generateAnswerField");
            $("#allField input[id='zgaduj']").each(function () {
                $(this).prop('disabled', true);
            });
        }
        if (receivedJSON.miss === receivedJSON.maxMoves) {
            $("<p>Niestety porażka ;/</p>").insertAfter(".generateAnswerField");
            $("#allField input[id='zgaduj']").each(function () {
                $(this).prop('disabled', true);
            });
        }
    });
});